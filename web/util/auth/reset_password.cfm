<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<cfif isdefined("form.submit")>
			<cfquery name="updatePassword" datasource="#session.DB_Core#">
				UPDATE users SET password='#hash(form.password)#' WHERE id=#form.user_id#
			</cfquery>

			<p style="color:red;"><strong>User ID <cfoutput>#form.user_id#</cfoutput> has a new password</strong></p>
		</cfif>
		<cfquery name="getUsers" datasource="#session.DB_Core#">
			SELECT id, username FROM users ORDER BY username
		</cfquery>
		<h1>Reset Password</h1>
		<form action="/util/auth/reset_password.cfm" method="POST">
			<label>Select User: <select name="user_id">
				<cfoutput query="#getUsers#">
					<option value="#id#">#username#</option>
				</cfoutput>
			</select></label>

			<br />

			<label>New Password: <input type="password" name="password"></label>

			<br />

			<input type="submit" name="submit" value="Submit">
		</form>
	</body>
</html>