/*
 * paf_debug.js
 *  Debugging routines for Prefiniti Application Framework
 *
 *  John Willis
 *  john@prefiniti.com
 * 
 *  Created: 30 April 2008
 *
 */
 writeConsole("Core Ready.");
 
 function showConsole()
 {
	 $('prefiniti_console_wrapper').Show();
 }

 function hideConsole()
 {
	 $('prefiniti_console_wrapper').Hide();
 }
 
 function writeConsole(msg)
 {
	 try {
	 var oldText;
	 
	 oldText = $('prefiniti_console').HTML();
	 $('prefiniti_console').HTML(oldText + '<br />' + msg);
	 
	 if($('auto_scroll').Checked()) {
		 var objDiv = $("prefiniti_console").Get();
		 objDiv.scrollTop = objDiv.scrollHeight;
	 }
	 } catch (ex) {}
 }
 
 function cls()
 {
	$('prefiniti_console').HTML('');
	var objDiv = $("prefiniti_console").Get();
	objDiv.scrollTop = objDiv.scrollHeight;
 }