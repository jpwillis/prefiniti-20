function PrefinitiDOMElement (id) 
{
	this.ElementID = id;
	this.Element = document.getElementById(id);
	
	return this;
}

PrefinitiDOMElement.prototype.Get = function () {
	return this.Element;
};

PrefinitiDOMElement.prototype.Destroy = function () {
	this.Parent().Element.removeChild(this.Element);
};

PrefinitiDOMElement.prototype.Parent = function () {
	var parentElID = this.Element.parentElement.id;
	
	return PrefinitiDOMElement(parentElID);
};

PrefinitiDOMElement.prototype.FirstChild = function () {
	var childID = this.Element.firstElementChild.id;
	
	return PrefinitiDOMElement(childID);
};

PrefinitiDOMElement.prototype.HTML = function(value) {
	if(value) {
		this.Element.innerHTML = value;
	}
	
	return this.Element.innerHTML;
};

PrefinitiDOMElement.prototype.Checked = function(value) {
	if(value) {
		this.Element.checked = value;
	}
	
	return this.Element.checked;
};

PrefinitiDOMElement.prototype.Enabled = function(value) {
	if(value) {
		if(value === true) {
			this.Element.disabled = false;
		}
		else {
			this.Element.disabled = true;
		}
		
		if(this.Element.disabled === true) {
			return false;
		}
		else {
			return true;
		}
	}
};

PrefinitiDOMElement.prototype.Value = function(value) {
	if(value) {
		this.Element.value = value;
	}
	
	return this.Element.value;
};

PrefinitiDOMElement.prototype.Hide = function() {
	this.Element.style.display = "none";
	
	return this;
};

PrefinitiDOMElement.prototype.Show = function() {
	try {
	this.Element.style.display = "inline";
	}
	catch (ex) { console.log("%o", ex);}
	return this;
};

var $ = function (id) {
	return new PrefinitiDOMElement(id);
};