<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<cfquery name="GetBase" datasource="#session.DB_Core#">
		SELECT * FROM basescripts WHERE RunAt <= 2 AND Enabled=1 ORDER BY RunAt, id
	</cfquery>

	<title>The Prefiniti Network</title>


</head>



	<body style="background-color:#2957A2;" id="Sfff" onResize="handleAppResize();" onLoad="LoadHandler();">
		<cfinclude template="/console.cfm">
		<div id="soundmanager-debug" style="display:none; position:absolute; left:0px; top:0px; z-index:50000; width:300px; height:auto;"></div>
		<div id="dev-null" style="display:none"></div>
    	<div id="PGlobalScreen"></div>

		<script src="/framework/UI/wz_tooltip.js" type="text/javascript"></script>
		<link rel="stylesheet" href="/Framework/CoreSystem/Styles/Base.css" type="text/css">
		<script src="/Framework/CoreSystem/CodeTransport.js" type="text/javascript"></script>
		<!--- <script src="/Framework/CoreSystem/LegacySupport.js" type="text/javascript"></script> --->
		<script src="/Framework/CoreSystem/DOM.js"></script>
		<script src="/framework/components/paf_debug.js" type="text/javascript"></script>
		<script src="/sm2/script/soundmanager2.js" type="text/javascript"></script>
        <cfoutput query="GetBase">
	        <script src="#ScriptPath#"></script>
        </cfoutput>
	<cfoutput>
	<script>
		var HP_CGI_Browser = '#CGI.HTTP_USER_AGENT#';
		var HP_CGI_NetworkNode = '#CGI.REMOTE_ADDR#';

		var HP_Browser = null;
		var HP_OS = null;

		var HPB_FIREFOX = 'Mozilla Firefox';
		var HPB_MSIE = 'Microsoft Internet Explorer';
		var HPB_SAFARI = 'Apple Safari';
		var HPB_OPERA = 'Opera';

		var HPOS_WINDOWS = 'Microsoft Windows';
		var HPOS_MACOS = 'MacOS';
		var HPOS_LINUX = 'Linux';
		var HPOS_OS2 = 'IBM OS/2';

		var HP_PrefinitiHostKey = '#session.PrefinitiHostKey#';
		var I_Mode = '#session.InstanceMode#';
		var I_Name = '#session.InstanceName#';

		function LoadHandler()
		{
			var imgWidth = 484;
			var imgHeight = 450;
			var screenWidth = 0;
			var screenHeight = 0;

			if (window.innerWidth) {
				screenWidth = window.innerWidth;
				screenHeight = window.innerHeight;
			}
			else if (document.all) {
				screenWidth = document.body.clientWidth;
				screenHeight = document.body.clientHeight;
			}

			var leftPos = (screenWidth / 2) - (imgWidth / 2);
			var leftPosX = (screenWidth / 2) - (imgWidth / 2);

			if(HP_CGI_Browser.indexOf('Firefox') != -1) {
				HP_Browser = HPB_FIREFOX;
			}

			if(HP_CGI_Browser.indexOf('Internet Explorer') != -1) {
				HP_Browser = HPB_MSIE;
			}

			if(HP_CGI_Browser.indexOf('Safari') != -1) {
				HP_Browser = HPB_SAFARI;
			}

			if(HP_CGI_Browser.indexOf('Opera') != -1) {
				HP_Browser = HPB_OPERA;
			}

			if(HP_CGI_Browser.indexOf('Windows') != -1) {
				HP_OS = HPOS_WINDOWS;
			}

			if(HP_CGI_Browser.indexOf('Mac') != -1) {
				HP_OS = HPOS_MACOS;
			}

			if(HP_CGI_Browser.indexOf('Linux') != -1) {
				HP_OS = HPOS_LINUX;
			}

			if(HP_CGI_Browser.indexOf('OS/2') != -1) {
				HP_OS = HPOS_OS2;
			}

			if(!HP_Browser) {
				HP_Browser = HPB_MSIE;
			}

			if (I_Mode != 'Development') {
				hideDiv('prefiniti_console_wrapper');
			}

			soundManager.onload = InitSounds;

			WMInitialize();
			WaitCursor();
			var AuthenticationRecord = {
				Theme: "Fluid",
				Username: "Guest",
				UserID: 0
			};

			var thisDesktop = new Desktop(AuthenticationRecord, TM_OPEN);

			DTInstall(thisDesktop);
			return;


		} /* LoadHandler() */
	</script>
	</cfoutput>

	</body>



</html>