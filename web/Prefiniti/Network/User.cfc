component accessors=true output=false persistent=false extends=Prefiniti.Framework {

	this.Written = false;
	this.Initialized = false;
	this.UserRecord = StructNew();

	public component function Create(struct UserRecord, string RawPassword)
	{

		this.Initialized = false;

		if(!StructKeyExists(Arguments.UserRecord, "Username")) {
			throw("User record must contain a Username key.");
		}
		else {
			this.UserRecord = Arguments.UserRecord;
			this.UserRecord.PasswordHash = Hash(Arguments.RawPassword);

			this.Initialized = true;
			this.Written = false;
		}

		return this;
	}

	public component function Save()
	{

		if(!this.Initialized) {
			throw("User record must be initialized before saving.");
		}

		try {
			var glob = CreateObject("lib.cfmumps.global");
			glob.open("Prefiniti", ["Users", this.UserRecord.Username]);
			glob.setObject(this.UserRecord);
			glob.close();

			this.Written = true;
		}
		catch (ex) {
			this.Written = false;

			throw("Database exception while attempting to save user account.");
		}

		return this;
	}

	public component function ResetPassword(required string OriginalPassword, required string NewPassword)
	{
		if(!this.Initialized) {
			throw("User record must be initialized before resetting password.");
		}

		if(Hash(OriginalPassword) != This.UserRecord.PasswordHash) {
			throw("Original password incorrect.");
		}
		else {
			this.UserRecord.PasswordHash = Hash(NewPassword);
		}
	}

	public component function Open(string Username)
	{
		var glob = CreateObject("lib.cfmumps.global");
		glob.open("Prefiniti", ["Users", arguments.Username]);

		if(!glob.defined().defined) {
			throw("Account does not exist.");
		}
		else {
			this.UserRecord = glob.getObject();
			this.Written = true;
			this.Initialized = true;
			glob.close();
		}

		return this;
	}

	public component function Authenticate(string Password)
	{
		if(!this.Initialized) {
			throw("User object not initialized.");
		}
		else {
			if(Hash(arguments.Password) != this.UserRecord.PasswordHash) {
				throw("Invalid credentials.");
			}
		}

		return this;
	}

	public component function Delete()
	{
		if(!this.Initialized) {
			throw("User object not initialized.");
		}
		else {
			try {
				var glob = CreateObject("lib.cfmumps.global");
				glob.open("Prefiniti", ["Users", this.UserRecord.Username]);
				glob.delete();
			}
			catch (ex) {
				throw("Database exception while attempting to delete user account.")
			}

			this.Written = false;
		}

		return this;
	}

}
