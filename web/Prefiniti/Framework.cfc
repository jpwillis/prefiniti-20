component accessors=true output=false persistent=false {

	public struct function StartupScripts () 
	{
		var glob = CreateObject("component", "lib.cfmumps.global");
		var output = StructNew();
		
		try {
			glob.open("Prefiniti", ["Config", "Startup", "Scripts"]);
			output = glob.getObject();
			glob.close();
		}
		catch (ex) {
			throw("Prefiniti.Framework.StartupScripts(): Database exception occurred");
		}
				
		return output;
	}

}