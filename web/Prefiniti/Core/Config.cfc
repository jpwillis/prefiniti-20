component accessors=true output=false persistent=false extends=Prefiniti.Framework {

	public struct function GetSystemSettingsObject(required string Key)
	{
		var output = StructNew();
		var glob = CreateObject("component", "lib.cfmumps.global");

		try {
			glob.open("Prefiniti", ["Config", Arguments.Key]);
			output = glob.getObject();
		}
		catch (ex) {
			throw("Prefiniti.Core.Config.GetSystemSettingsObject(): Database exception occurred");
		}


		return output;
	}

}