<cfcomponent displayname="Application" output="true" hint="Application">

	<cfset this.Name="Prefiniti">
	<cfset this.ApplicationTimeout = CreateTimeSpan(1, 0, 0, 0)>
	<cfset this.SessionManagement = true>
	<cfset this.SetClientCookies = true>

	<cfsetting requesttimeout="120" showdebugoutput="true" enablecfoutputonly="false">

	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first created.">

		<cfreturn true>
	</cffunction>


	<cffunction name="OnSessionStart" access="public" returntype="void" output="false" hint="Fires when the session is first created.">

		<cfset iniFile = expandPath("/Instance/Prefiniti.ini")>
		<cfset session.InstanceName = getProfileString(iniFile, "Instance", "InstanceName")>
		<cfset session.InstanceRoot = getProfileString(iniFile, "Instance", "InstanceRoot")>

		<cfset session.UserRoot = getProfileString(iniFile, "CMS", "UserRoot")>
		<cfset session.SiteRoot = getProfileString(iniFile, "CMS", "SiteRoot")>
		<cfset session.UserRootURL = getProfileString(iniFile, "CMS", "UserRootURL")>
		<cfset session.SiteRootURL = getProfileString(iniFile, "CMS", "SiteRootURL")>
		<cfset session.InstanceErrorReporting = getProfileString(iniFile, "Instance", "ErrorReporting")>
		<cfset session.InstanceMode = getProfileString(iniFile, "Instance", "Mode")>
		<cfset session.InstanceRegion = getProfileString(iniFile, "Instance", "Region")>
		<cfset session.InstanceNotificationAccount = getProfileString(iniFile, "Instance", "NotificationAccount")>

		<cfset session.DB_Core = getProfileString(iniFile, "Database", "CoreSchema")>
		<cfset session.DB_Sites = getProfileString(iniFile, "Database", "SitesSchema")>
		<cfset session.DB_CMS = getProfileString(iniFile, "Database", "CMSSchema")>
		<cfset session.DPnlRoot = "/Framework/CoreSystem/Widgets/DPnl">
		<cfset session.PrefinitiHostKey = "">

		<cfreturn>
	</cffunction>


	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="false" hint="Fires at first part of page processing.">
 		<cfargument name="TargetPage" type="string" required="true">

		<cfreturn true>
	</cffunction>

	<cffunction name="OnRequest" access="public" returntype="void" output="true" hint="Fires after pre page processing is complete.">
 		<cfargument name="TargetPage" type="string" required="true">

		<cfinclude template="#arguments.TargetPage#">

		<cfreturn>
	</cffunction>


	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true" hint="Fires after the page processing is complete.">

		<cfreturn >
	</cffunction>

	<cffunction name="OnSessionEnd" access="public" returntype="void" output="false" hint="Fires when the session is terminated.">
		<cfargument name="SessionScope" type="struct" required="true">
 		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#">

		<cfreturn>
	</cffunction>

	<cffunction name="OnApplicationEnd" access="public" returntype="void" output="false" hint="Fires when the application is terminated.">
		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#">

		<cfreturn >
	</cffunction>
<!---
	<cffunction name="OnError" access="public" returntype="void" output="true" hint="Fires when an exception occures that is not caught by a try/catch.">
		<cfargument name="Exception" type="any" required="true">
		<cfargument name="EventName" type="string" required="false" default="">

		<!---<cferror type="exception" exception="any" mailto="#session.InstanceErrorReporting#" template="/Framework/CoreSystem/HTMLResources/ExceptionTrap.cfm">--->

		<cfreturn>
	</cffunction>
--->
</cfcomponent>